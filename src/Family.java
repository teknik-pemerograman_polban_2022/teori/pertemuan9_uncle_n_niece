import java.util.*;

public class Family {

    private static TreeSet<Niece> Ponakan = new TreeSet<Niece>();
    private static TreeSet<Uncle> Paman = new TreeSet<Uncle>();

    // Getter
    public static Set<Uncle> getUncles() {
        return Paman;
    }

    public static Set<Niece> getNieces() {
        return Ponakan;
    }

    boolean addNiece(java.lang.String name, int day, int month) {
        Niece temp = new Niece(name, day, month);
        if (Ponakan.contains(temp)) {
            return false;
        }
        Ponakan.add(temp);
        return true;
    }

    boolean addUncle(java.lang.String name) {
        Uncle temp = new Uncle(name);
        if (Paman.contains(temp)) {
            return false;
        }
        Paman.add(temp);
        return true;
    }

    static Niece findNiece(java.lang.String name) {
        Iterator<Niece> iterator = Ponakan.iterator();
        while (iterator.hasNext()) {
            Niece node = iterator.next();
            if (node.getName().compareTo(name) == 0)
                return node;
        }
        return null;
    }

    static Uncle findUncle(java.lang.String name) {
        Iterator<Uncle> iterator = Paman.iterator();
        while (iterator.hasNext()) {
            Uncle node = iterator.next();
            if (node.getName().compareTo(name) == 0)
                return node;
        }
        return null;
    }

    void listUncle() {
        Iterator<Uncle> iterator = Paman.iterator();
        int no = 1;
        while (iterator.hasNext()) {
            Uncle node = iterator.next();
            System.out.println((no++) + ". " + node.getName());
        }
    }

    void listNiece() {
        TreeSet<Niece> PonakanByDate = new TreeSet<Niece>(
                Comparator.comparing(Niece::getDay).thenComparing(Niece::getMonth));

        Iterator<Niece> iterator = Ponakan.iterator();
        while (iterator.hasNext()) {
            Niece node = iterator.next();
            PonakanByDate.add(node);
        }

        Iterator<Niece> iterator2 = PonakanByDate.iterator();
        int no = 1;
        while (iterator2.hasNext()) {
            Niece node = iterator2.next();
            System.out.println((no++) + ". " + node.getName() + " -> " + node.getDay() + "/" + node.getMonth());
        }
    }

}
