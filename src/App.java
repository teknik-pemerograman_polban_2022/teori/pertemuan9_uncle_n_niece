import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import com.google.gson.Gson;

public class App {

    public static void main(String[] args) throws Exception {

        Scanner read = new Scanner(System.in);
        String namaPonakan, namaPaman, hadiah;
        int hari, bulan, aksi;
        boolean loop = true;

        Family happy = new Family();

        happy.addNiece("Sari", 18, 2);
        happy.addNiece("Sary", 12, 1);

        happy.addUncle("Bobi");
        happy.addUncle("Bobi");
        happy.addUncle("Asep");

        while (loop) {
            System.out.println("========== Daftar Niece ==========");
            happy.listNiece();
            System.out.println("\n========== Daftar Uncle ==========");
            happy.listUncle();

            System.out.print(
                    "\nMasukan Aksi:\n1. Tambah Niece\n2. Tambah Uncle\n3. Berikan Hadiah\n4. Tampil hadiah yang dikirim oleh...\n5. Tampil hadiah yang diterima oleh...\n6. Hapus semua hadiah yang diberikan pada...\n7. Exit\n: ");
            aksi = read.nextInt();

            switch (aksi) {
                case 1:
                    System.out.print("Masukan nama Niece : ");
                    namaPonakan = read.next();
                    System.out.print("\nMasukan tanggal lahir : ");
                    hari = read.nextInt();
                    System.out.print("\nMasukan bulan lahir : ");
                    bulan = read.nextInt();
                    if (happy.addNiece(namaPonakan, hari, bulan) == true) {
                        System.out.println("Proses penambahan data ponakan [" + namaPonakan + "] berhasil!\n");
                    } else {
                        System.out
                                .println(
                                        "Proses penambahan data ponakan [" + namaPonakan + "] gagal, nama duplikat!\n");
                    }
                    break;
                case 2:
                    System.out.print("Masukan nama Uncle : ");
                    namaPaman = read.next();
                    if (happy.addUncle(namaPaman) == true) {
                        System.out.println("\nProses penambahan data paman [" + namaPaman + "] berhasil!\n");
                    } else {
                        System.out
                                .println("\nProses penambahan data paman [" + namaPaman + "] gagal, nama duplikat!\n");

                    }
                    break;
                case 3:
                    System.out.print("Masukan nama pengirim (Uncle) : ");
                    namaPaman = read.next();
                    System.out.print("\nMasukan nama Hadiah : ");
                    hadiah = read.next();
                    System.out.print("\nMasukan nama penerima (Niece) : ");
                    namaPonakan = read.next();
                    if (happy.findUncle(namaPaman) != null && happy.findNiece(namaPonakan) != null) {
                        if (happy.findUncle(namaPaman).addPresent(happy.findNiece(namaPonakan), hadiah) == true) {
                            happy.findNiece(namaPonakan).receivePresent(happy.findUncle(namaPaman), hadiah);
                            System.out.println("\nProses penambahan hadiah [" + hadiah + "] berhasil!\n");
                        } else {
                            System.out
                                    .println("\nProses penambahan hadiah [" + hadiah
                                            + "] gagal, hadiah sudah diberikan ke Niece lain!\n");
                        }
                    } else {
                        System.out.println("\nUncle atau Niece tidak ditemukan!");
                    }
                    break;
                case 4:
                    System.out.print("Masukan nama pengirim (Uncle): ");
                    namaPaman = read.next();
                    if (happy.findUncle(namaPaman) != null) {
                        happy.findUncle(namaPaman).listPresents();
                    } else {
                        System.out.println("\nUncle tidak ditemukan!");
                    }
                    break;
                case 5:
                    System.out.print("\nMasukan nama penerima (Niece): ");
                    namaPonakan = read.next();
                    if (happy.findNiece(namaPonakan) != null) {
                        happy.findNiece(namaPonakan).listPresents();
                    } else {
                        System.out.println("\nPonakan tidak ditemukan!");
                    }
                    break;

                case 6:
                    System.out.print("\nMasukan nama ponakan (Niece): ");
                    namaPonakan = read.next();
                    if (happy.findNiece(namaPonakan) != null) {
                        happy.findNiece(namaPonakan).clearPresents();
                        final String temp_name = namaPonakan;
                        Family.getUncles().forEach((uncle) -> {
                            uncle.removePresent(Family.findNiece(temp_name));
                        });
                    } else {
                        System.out.println("\nPonakan tidak ditemukan!");
                    }
                    break;

                case 7:
                    loop = false;
                    break;
            }

            System.out.println("\nPress Enter key to continue...");
            try {
                System.in.read();
            } catch (Exception e) {
            }
        }
        read.close();
    }
}
